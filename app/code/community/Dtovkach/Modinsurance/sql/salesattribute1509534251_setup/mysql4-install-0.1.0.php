<?php
$installer = $this;
$installer->startSetup();

//$installer->addAttribute("shipment_item", "insurance", array("type"=>"varchar"));
//$installer->addAttribute("quote_address", "insurance_total", array("type"=>"varchar"));
//$installer->addAttribute("order", "insurance_total", array("type"=>"varchar"));


$installer->addAttribute("quote", "insurance", array("type"=>"varchar"));
$installer->addAttribute("quote_address", "insurance", array("type"=>"varchar"));
$installer->addAttribute("order", "insurance", array("type"=>"varchar"));



$installer->addAttribute("quote", "insurance_shipping_method", array("type"=>"varchar"));
$installer->addAttribute("quote_address", "insurance_shipping_method", array("type"=>"varchar"));
$installer->addAttribute("order", "insurance_shipping_method", array("type"=>"varchar"));




$installer->endSetup();
