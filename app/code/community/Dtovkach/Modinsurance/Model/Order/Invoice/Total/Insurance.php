<?php
class Dtovkach_Modinsurance_Model_Order_Invoice_Total_Insurance
extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
		$order=$invoice->getOrder();
        $orderInsuranceTotal = $order->getInsuranceTotal();
        if ($orderInsuranceTotal&&count($order->getInvoiceCollection())==0) {
            $invoice->setGrandTotal($invoice->getGrandTotal()+$orderInsuranceTotal);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal()+$orderInsuranceTotal);
        }
        return $this;
    }
}