<?php

/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Order_Creditmemo_Total_Insurance
extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {

		return $this;

        $order = $creditmemo->getOrder();
        $orderInsuranceTotal        = $order->getInsuranceTotal();

        if ($orderInsuranceTotal) {
			$creditmemo->setGrandTotal($creditmemo->getGrandTotal()+$orderInsuranceTotal);
			$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal()+$orderInsuranceTotal);
        }

        return $this;
    }
}
