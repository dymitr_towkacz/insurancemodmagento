<?php


/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Newordertotalobserver
{


		 public function checkoutControllerOnepageSaveShippingMethod(Varien_Event_Observer $observer)
    {
        /** @var $helper Dtovkach_Modinsurance_Helper_Data $helper */
        $helper   = Mage::helper('dtovkach_modinsurance');
        $accepted = $observer->getRequest()->getParam('shippinginsurance_enabled', false);

        $quote           = $observer->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $shippingMethod  = $shippingAddress->getShippingMethod();
        $shippingRates   = $shippingAddress->collectShippingRates()->getGroupedAllShippingRates();

        $shippingAddress->setInsuranceShippingMethod(null);
        $shippingAddress->setInsurance(0);

        $quote->setInsuranceShippingMethod(null);
        $quote->setInsurance(0);

        if (!$helper->isFeatureEnabled() || !$accepted) {
            return $this;
        }

        $carrierCode = null;

        foreach ($shippingRates as $rate) {
            foreach ($rate as $carrier) {
                if ($shippingMethod === $carrier->getCode()) {
                    $carrierCode = $carrier->getCarrier();
                }
            }
        }

  
        if (!$carrierCode) {
            return $this;
        }

        $allowed = $helper->isCarrierCodeAllowed($carrierCode);

        if (!$allowed) {
            return $this;
        }

        $cost = $helper->calculateInsuranceCost($carrierCode, $quote->getSubtotal());

        $shippingAddress->setInsuranceShippingMethod($carrierCode);
        $shippingAddress->setInsurance($cost);

        $quote->setInsuranceShippingMethod($carrierCode);
        $quote->setInsurance($cost);

        return $this;
    }
}
