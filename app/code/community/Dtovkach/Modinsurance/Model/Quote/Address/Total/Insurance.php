<?php
class Dtovkach_Modinsurance_Model_Quote_Address_Total_Insurance 
extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
     public function __construct()
    {
         $this -> setCode('insurance_total');
         }
    /**
     * Collect totals information about insurance
     * 
     * @param Mage_Sales_Model_Quote_Address $address 
     * @return Mage_Sales_Model_Quote_Address_Total_Shipping 
     */
     public function collect(Mage_Sales_Model_Quote_Address $address)
    {
         parent :: collect($address);
         $items = $this->_getAddressItems($address);
         if (!count($items)) {
            return $this;
         }
         $quote= $address->getQuote();

		 //amount definition

         $insuranceAmount = 0.00;

         //amount definition

         $insuranceAmount = $quote -> getStore() -> roundPrice($insuranceAmount);
         $this -> _setAmount($insuranceAmount) -> _setBaseAmount($insuranceAmount);
         $address->setData('insurance_total',$insuranceAmount);

         return $this;
     }
    
    /**
     * Add insurance totals information to address object
     * 
     * @param Mage_Sales_Model_Quote_Address $address 
     * @return Mage_Sales_Model_Quote_Address_Total_Shipping 
     */
     public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
         parent :: fetch($address);
         $amount = $address -> getTotalAmount($this -> getCode());
         if ($amount != 0){
             $address -> addTotal(array(
                     'code' => $this -> getCode(),
                     'title' => $this -> getLabel(),
                     'value' => $amount
                    ));
         }
        
         return $this;
     }
    
    /**
     * Get label
     * 
     * @return string 
     */
     public function getLabel()
    {
         return Mage :: helper('modinsurance') -> __('Insurance');
    }
}