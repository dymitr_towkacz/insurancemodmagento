<?php
/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Setting_Cloned extends Mage_Core_Model_Config_Data
{
    public function getPrefixes()
    {
        $prefixes = array();
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();

        /** @var  $carrier Mage_Shipping_Model_Carrier_Abstract */
        foreach ($carriers as $carrier) {
            $prefixes[] = array(
                'field' => sprintf('field_%s_', $carrier->getCarrierCode()),
                'label' => $carrier->getConfigData('title'),
            );
        }

        return $prefixes;
    }
}
