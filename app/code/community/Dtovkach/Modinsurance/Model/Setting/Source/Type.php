<?php
/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Setting_Source_Type
{
    const TYPE_FIXED_ID  = 1;
    const TYPE_FIXED_HDL = 'Fix. amount';

    const TYPE_ORDER_PERCENTAGE_ID  = 2;
    const TYPE_ORDER_PERCENTAGE_HDL = 'Percentage of amount';

    public function toOptionArray()
    {
        $options = array();

        foreach ($this->getTypeIdHdlMap() as $value => $label) {
            $options[] = array('value' => $value, 'label' => $label);
        }

        return $options;
    }

    public function getTypeIdHdlMap()
    {
        return array(
            self::TYPE_FIXED_ID            => self::TYPE_FIXED_HDL,
            self::TYPE_ORDER_PERCENTAGE_ID => self::TYPE_ORDER_PERCENTAGE_HDL,
        );
    }
}
