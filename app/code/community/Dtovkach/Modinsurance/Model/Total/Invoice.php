<?php

/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Total_Invoice extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        /** @var $helper Dtovkach_Modinsurance_Helper_Data $helper */
        $helper           = Mage::helper('dtovkach_modinsurance');
        $isFeatureEnabled = $helper->isFeatureEnabled();
        $order            = $invoice->getOrder();
        $costInsurance    = $order->getInsurance();

        if ($isFeatureEnabled && $order->getInsuranceShippingMethod()) {
            $invoice->setGrandTotal($invoice->getGrandTotal() + $costInsurance);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $costInsurance);
        }

        return $this;
    }
}
