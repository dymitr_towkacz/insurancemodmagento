<?php

/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Total_Creditmemo extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        /** @var $helper Dtovkach_Modinsurance_Helper_Data $helper */
        $helper           = Mage::helper('dtovkach_modinsurance');
        $isFeatureEnabled = $helper->isFeatureEnabled();
        $order            = $creditmemo->getOrder();
        $costInsurance    = $order->getInsurance();

        if ($isFeatureEnabled && $order->getInsuranceShippingMethod()) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $costInsurance);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $costInsurance);
        }

        return $this;
    }
}
