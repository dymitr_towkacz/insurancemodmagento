<?php

/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Model_Total_Quote extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'insurance';

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        /** @var $helper Dtovkach_Modinsurance_Helper_Data */
        $helper = Mage::helper('dtovkach_modinsurance');

        if ($helper->isFeatureEnabled()) {
            $items = $this->_getAddressItems($address);

            if (!count($items)) {
                return $this;
            }

            if ($address->getInsuranceShippingMethod() && $address->getInsurance()) {
                $address->setGrandTotal($address->getGrandTotal() + $address->getInsurance());
                $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getInsurance());

                $quote = $address->getQuote();
                $quote->setGrandTotal($quote->getGrandTotal() + $quote->getShippingInsurance());
                $quote->setBaseGrandTotal($quote->getBaseGrandTotal() + $quote->getInsurance());
            }
        }

        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        /** @var $helper Dtovkach_Modinsurance_Helper_Data */
        $helper = Mage::helper('dtovkach_modinsurance');

        if ($address->getInsuranceShippingMethod()) {
            $costInsurance = $address->getInsurance();
            $address->addTotal(
                [
                    'code'  => $this->getCode(),
                    'title' => $helper->getTranslatedLabel(),
                    'value' => $costInsurance
                ]
            );
        }

        return $this;
    }
}
