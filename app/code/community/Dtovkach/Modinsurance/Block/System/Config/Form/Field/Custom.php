<?php
/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Block_System_Config_Form_Field_Custom
    extends Mage_Adminhtml_Block_System_Config_Form_Field
    implements Varien_Data_Form_Element_Renderer_Interface
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $prototype = $element->getData('field_config');

        if ($prototype && $prototype instanceof Mage_Core_Model_Config_Element && isset($prototype->label)) {
            $element->setLabel($prototype->label);
        }

        return parent::render($element);
    }
}
