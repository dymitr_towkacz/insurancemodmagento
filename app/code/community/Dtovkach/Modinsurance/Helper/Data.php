<?php
/**
 * @category   Dtovkach
 * @package    Dtovkach_Modinsurance
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Dmitriy Tovkach <d.tovkach@itransition.com>
 */

class Dtovkach_Modinsurance_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getTranslatedLabel()
	{
			return $this->__('Shipping DT Insurance');
	}

	public function isFeatureEnabled()
	{
			return (bool) Mage::getStoreConfig('insurance/settings/enabled');
	}

	public function isCarrierCodeAllowed($carrierCode)
	{
			if (!is_string($carrierCode) || empty($carrierCode)) {
					return false;
			}

			return (bool) Mage::getStoreConfig(sprintf('insurance/rates/field_%s_enabled',$carrierCode));
	}

	public function calculateInsuranceCost($carrierCode, $subTotal)
	{
			if (!is_string($carrierCode) || empty($carrierCode)) {
					return 0;
			}

			$type = Mage::getStoreConfig('insurance/rates/field_' . $carrierCode . '_type');
			$fee  = Mage::getStoreConfig('insurance/rates/field_' . $carrierCode . '_fee');

			switch ($type) {
					case Dtovkach_Modinsurance_Model_Setting_Source_Type::TYPE_FIXED_ID: {
							return round($fee, 2, PHP_ROUND_HALF_UP);
					}
					case Dtovkach_Modinsurance_Model_Setting_Source_Type::TYPE_ORDER_PERCENTAGE_ID: {
							return round($subTotal * ($fee / 100), 2, PHP_ROUND_HALF_UP);
					}
					default:
							return 0;
			}
	}

	public function rewriteTotals($class)
	{
			$order = $class->getOrder();

			if ($order->getInsuranceShippingMethod()) {
					$costInsurance = $order->getInsurance();
					$class->addTotalBefore(
							new Varien_Object(
									array(
											'code'       => $class->getCode(),
											'value'      => $costInsurance,
											'base_value' => $costInsurance,
											'label'      => $this->getTranslatedLabel()
									),
									'grand_total'
							)
					);
			}
	}
}
